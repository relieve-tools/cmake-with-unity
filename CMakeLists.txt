# CMakeList.txt : CMake project for pmi.
# Set minimal cmake requirement.
cmake_minimum_required(VERSION 3.16)

# Add toolchain
set(CMAKE_TOOLCHAIN_FILE "toolchain.cmake")
set(LIBRARY_NAME "template")

# Define project.
project (${LIBRARY_NAME} VERSION 0.1 LANGUAGES C)
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED TRUE)

# Add source to this project's static library.
file(GLOB SOURCE_FILES "src/*.c")
add_library(${PROJECT_NAME} STATIC ${SOURCE_FILES})

# Set version property.
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION ${PROJECT_VERSION})

# Define public headers.
target_include_directories(${PROJECT_NAME} PUBLIC "include")
target_include_directories(${PROJECT_NAME} PRIVATE "src")

# Add math library.
target_link_libraries(${PROJECT_NAME} PRIVATE m)

# Enable Ctest.
enable_testing()
include(Dart)

# Add unit testing
add_subdirectory("external")
add_subdirectory("test")
